#!/usr/bin/python3

from xml.sax.handler import ContentHandler #clase general de reconocedores de sax
from xml.sax import make_parser
import urllib.request

class CounterHandler(ContentHandler):

    def __init__ (self):
        self.inContent = False 
        self.inItem=False  
        self.theContent = ""

    def startElement (self, name, attrs): 
        if name == 'item': 
            self.inItem=True 
        elif self.inItem:    
            if name == 'title': 
                self.inContent = True
            elif name == 'link': 
                self.inContent = True

    def endElement (self, name): 
        if self.inItem:
            if name=="title":
                self.title=self.theContent
                print("<li>TITLE: "+ str(self.title) + "</li>")
            elif name=="link":
                self.link=self.theContent
                lines=('<li>LINK: <a href="'+ str(self.link) + '">' + str(self.link) + '</a></li>' + '\n') # pongo el segundo str(self.link) para que se vea la url, ya 
                # que si pongo str(self.title) se ve el titulo y me lleva a la noticia
                print(str(lines))
            elif name=="item":
                self.inItem=False  # para que cierre la etiqueta del último ítem y no meta más títulos ni links de otros que no estén bajo el raíz que es ítem  

        if self.inContent:
            self.inContent=False
            self.theContent=""                  

    def characters (self, chars):    
        if self.inContent:
            self.theContent = self.theContent + chars #meto lo leido

JokeParser = make_parser() 
JokeHandler = CounterHandler() 
JokeParser.setContentHandler(JokeHandler) 

xmlFile=urllib.request.urlopen("http://barrapunto.com/index.rss")
JokeParser.parse(xmlFile) 

print ("Parse complete")